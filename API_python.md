# Conceptes bàsics

## Python

Python es un llenguatge de programació que s'utilitza, entre d'altres cosas, per fer scripts.

## API

Es una conexió entre una biblioteca de dades o arxius i una aplicació informática.

## Flask

Es un framework de python per crear aplicacions webs de manera senzilla.

# Creació d'una API en python3

## Fent servir Docker

Tindrem una carpeta de projecte amb una estructura com aquesta:

```
.
├── api.py
├── dockercompose.yml
├── Dockerfile
└── templates
    └── index.html

```

### api.py

On *"api"* pot ser qualsevol nom. És l'script que s'executarà per posar en marxa l'aplicació web.

Exemple:

```python
#!/usr/bin/python3

from flask import Flask, render_template

app = Flask(__name__)

@app.route("/")
def arrel():
    return render_template('index.html')

app.run(host="0.0.0.0", debug=True)

```

- La primera línia ```from flask import Flask, render_template``` és per importar el Flask. Per a que funcioni s'ha de tenir-ho instal·lat:

    ```pip3 install Flask```

- ```app = Flask(__name__)``` instancia l'objecte aplicació a Flask, amb el nom del módul que s'estarà utilitzant.

- ```@app.route("/")``` és una anotació que indica que tot el que está just a sota formarà part de la ruta que marca

    En aquest cas concret hi ha només una `"/"`, així que indicarà el que hi hagi a la arrel (accesible només indicant la IP o domini i el port). però en el cas d'haber-hi escrita una `"/ruta/recurs"`, s'hauria d'accedir amb `[IP o domini]:[port]/ruta/recurs`.

- ```def cosas():``` es una funció que s'executarà quan s'accedeixi a la ruta en concret, que estarà definida a sota d'aquesta línia. Retornarà un valor.

    En aquest cas, amb la línia `return render_template('index.html')` indiquem que retorni un html que està creat al directori del projecte, concretament dins de la carpeta "templates". Es pot accedir a aquest html a través d'un navegador escrivint amb la ruta adequada.

    Es pot fer que retorni d'altres cosas que no siguin pàgines html. Per exemple, per retornar un string només cal escriure'l entre cometes al costat del mètode *"return"*.

- ```app.run(host="0.0.0.0", debug=True)``` és el métode que s'encarrega d'executar l'aplicació. El paràmetre `host="0.0.0.0"` fa que sigui visible des d'altres màquines de la xarxa. El paràmetre `debug=True` permet que s'actualitzin els canvis al moment, a més de tenir una consola a navegador que permeti veure que està fent l'aplicació.

    Aquí també es poden indicar altres cosas com el port on s'executarà, que per defecte és el 5000.




Un cop acabat s'executarà amb la comanda:

```python3 api.py```

Resultarà en una sortida com aquesta:

```
 * Serving Flask app 'api' (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: on
 * Running on all addresses.
   WARNING: This is a development server. Do not use it in a production deployment.
 * Running on http://192.168.2.139:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 
```
On s'indica informació important la ruta per accedir-hi i el port o el PIN del debugger per si es vol fer servir la consola al navegador.

### dockercompose.yml

És un fitxer YAML que indica la configuració del docker compose. [Més informació aquí](https://gitlab.com/naomi.hidalgo/documentacio-instalacions/-/blob/main/docker.md#docker-compose).

Un exemple de *dockercompose.yml* on ens assegurem els ports d'entrada i de sortida:

```yaml
version: '3.7'
services:
  miapp:
    build: .
    container_name: container
    ports:
      - 5000:5000
```

### Dockerfile

El Dockerfile és un fitxer de configuració que serveix per a construir un docker preconfigurat y fer que s'executin ordres a l'executar-lo. [Més informació aquí](https://gitlab.com/naomi.hidalgo/documentacio-instalacions/-/blob/main/docker.md#dockerfile).

### templates

És un directori on es troben els diferents arxius HTML que s'importaran des de l'script.



## Interactuar amb la API

Es pot accedir a la API REST de diverses maneres, algunes d'aquestes són: a través d'un navegador web, mitjançant la comanda `curl` al terminal o per mitjà  d'una aplicació gràfica com Postman.

Es poden utilitzar mètodes HTTP

###