# GIT

GIT és un software de control de versions d'arxius, principalment utilitzat al desenvolupament codi font.

## Comandes básiques

#### git clone [URL del repositori]

Clona un repositori remot a la màquina local.

#### git init

Crea un nou repositori

### Modificar un repositori

#### git pull

Integra els canvis remots al repositori local, en el cas que hi hagin

#### git add [fitxers]

Afegeix fitxers al repositori local

#### git commit

Guarda els canvis al repositori local

#### git push

Puja els canvis del repositori local al remot

#### git status

Mostra l'estat del canvi actual

#### git log

Mostra un registre de tots els canvis

## Branques

Una branca és una divergència del codi principal. S'utilitza principalment quan es treballa en una nova part del projecte, per tal de not afegir codi inacabat o inestable a la branca principal.

#### git branch

Mostra per pantalla les diferents branques del repositori i la branca on es troba actualment

#### git checkout -b [nom de la branca]

Es passa a treballar a la branca nova indicada

#### git branch -D [nom de la branca]

Elimina la branca indicada


## Conflictes

---

## GitLab



### Issues

### Merge requests

> //TODO