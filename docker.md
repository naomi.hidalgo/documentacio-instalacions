# Docker

Docker és una aplicació open-source que permet la creació i l'ús de contenidors.

A diferència de les màquines virtuals, dins del Docker els contenidors comparteixen el mateix kernel.

## Instal·lació des del repositori

Esborrar qualsevol versió de *Docker* que tenim instal·lada si existeix:

` sudo apt-get remove docker docker-engine docker.io containerd runc `

Instal·lar els paquets necessaris per descarregar el *Docker* per repositori:

` sudo apt-get update `

`sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release `

` curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg `

` echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null `

 #### Instal·lar el motor de *Docker*:

` sudo apt-get update `

 ` sudo apt-get install docker-ce docker-ce-cli containerd.io ` 
 
Per poder executar totes les comandes de *Docker* amb usuari normal:

` sudo usermod -aG docker darta ` 


## Execució d'un container a *Docker*

Per exemple, per executar una [imatge de Nextcloud](https://hub.docker.com/_/nextcloud) :

 ` docker run -d -p 8080:80 --name minxt nextcloud  `

- On `-d`  indica que s'ha d'executar en segon pla

- `-p` indica el port on s'executa la imatge

- `--name` indica el nom que li donem, per no haver s'usar l'ID per identificar-la, en aquest cas 'minxt'

Podem comprovar que funciona en un client escrivint en un navegador:

 ` (IPservidor):(port) `

![nextcloud firefox](pictures/nextcloud_demo_scrn.png)

Per aturar aquest servidor es pot fer amb la comanda:

` docker stop minxt `


## *Docker Compose*

Docker Compose és una utilitat per automatitzar aplicacions de Docker a partir d'un fitxer YAML.

#### Instal·lació

Primer de tot s'haurà d'instal·lar el Docker Compose. Com és un paquet de python3, s'ha d'instal·lar amb pip3. En el cas de no tenir-ho instal·lat:

` sudo apt install python3-pip `

Instal·lar el Docker Compose:

` pip3 install docker-compose `

#### Configuració

Crear un fitxer YAML on s'indiqui els contenidors que es volen posar en marxa i les consfiguracions respectves.

Per exemple:

```
version: '2'

volumes:
  nextcloud:
  db:

services:
  db:
    image: mariadb
    restart: always
    command: --transaction-isolation=READ-COMMITTED --binlog-format=ROW
    volumes:
      - db:/var/lib/mysql
    environment:
      - MYSQL_ROOT_PASSWORD=SuperSecure
      - MYSQL_PASSWORD=SuperSecure
      - MYSQL_DATABASE=nextcloud
      - MYSQL_USER=nextcloud

  app:
    image: nextcloud
    restart: always
    ports:
      - 8080:80
    links:
      - db
    volumes:
      - nextcloud:/var/www/html
    environment:
      - MYSQL_PASSWORD=SuperSecure
      - MYSQL_DATABASE=nextcloud
      - MYSQL_USER=nextcloud
      - MYSQL_HOST=db
```


Si el fitxer s'anomena ` docker-composer.yml ` s'executarà amb la comanda

` docker-compose up `

si té un altre nom s'executarà:

` docker-compose up -f [nom de l'arxiu] `

Ara, en aquest exemple concret, deuríem de poder accedir al Nextcloud per mitjà del navegador amb la URL `(IPservidor):8080` i aquesta màquina hauria d'utilizar la base de dades de la imatge 'mariadb'  

Per aturar l'execucció dels contenidors al docker compose es pot utilitzar la comanda:

` docker-compose down `

## Dockerfile

Un Dockerfile és un document de text pla on són una sèrie de comandes que s'executaran succesivament al construir una imatge o a l'executar una imatge en concret.

Exemple de la sintaxi:

```
FROM debian:11    

RUN apt update

RUN apt install cowsay -y


CMD [ "/usr/games/cowsay", "bon dia" ]
```

No són les úniques, però les comandes que veiem en aquest exemple són:

**FROM:** Indica el nom de la imatge on es basa el futur docker muntat

**RUN:** Executa una comanda dins de la imatge al moment del build

**CMD:** Executa una comanda dins de la imatge al moment de la seva arrencada



---

### Comandes bàsiques

#### docker ps

Llista els contenidors que estan en marxa

#### docker image ls

Llista totes les imatges guardades 

#### docker rm [nom contenidor | ID contenidor]

Elimina un contenidor

#### docker pull [nom contenidor : tag]

Agafa una imatge local, si es troba un contenidor amb aquell nom a la carpeta, o remotament de hub.docker.com/ en cas de que no es trobi allà.

Al agafar aquesta imatge apareguerà disponible a la llista d'imatges.
 Es pot comprovar quines són les diferents versions al final de la web, a la pàgina de cada imatge.
#### docker run [nom contenidor | ID contenidor]

Posa en marxa un contenidor 

#### docker inspect [nom contenidor | ID contenidor]

Retorna informació del contenidor en format JSON

#### docker build 

Construeix un docker a partir d'un Dockerfile

##### docker build -t [imatge] [Dockerfile]

Construeix un docker amb els scripts d'un Dockerfile específic sobre una imatge específica


> //TODO

