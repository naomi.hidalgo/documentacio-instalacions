# Instal·lacions de programes

En aquest document són les instruccions necessàries per a instal·lar tots el programes que utilitzarem

## TMUX y TMATE

### TMUX

**Aplicació que permet terminals simúltanies en una finestra**

` sudo apt install tmux `

### TMATE

**Aplicació per compartir terminals**

` sudo apt install tmate `

## Typora

**Editor de markdown**

` sudo snap install typora `

## Postman

**Plataforma per interactuar amb APIs**

` sudo snap install postman `

## Geany

**IDE simple**

` sudo apt install geany `

## Element

**Client de missatgeria instantània**

Primer s'ha d'instal·lar 'flatpak':

` sudo apt install flatpak `

Per instal·lar l'element:

` flatpak install --from https://flathub.org/repo/appstream/im.riot.Riot.flatpakref `

## codium

**Clon de VS Code amb llicència lliure.**

` wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg `


` echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' | sudo tee /etc/apt/sources.list.d/vscodium.list `


` sudo apt update && sudo apt install codium `

## ohmyzsh

**Framework per a la shell alternativa 'zsh'**

S'ha de tenir 'zsh' instal·lat:

` sudo apt install zsh `

` sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)" `


## Terminator

` sudo apt install -y terminator `

## Guake

**Terminal d'escriptori desplegable**

` sudo apt install -y guake `

## Docker compose

[Més informació](https://gitlab.com/naomi.hidalgo/documentacio-instalacions/-/blob/main/docker.md)

Com és un paquet de python3, s'ha d'instal·lar 'pip3':


` sudo apt install python3-pip `

Desprès ya podem instal·lar el 'docker-compose' :

` pip3 install docker-compose `


---


## Configuració dels drivers WiFi
` sudo nano/etc/apt/sources.list `

Escriure 'non-free' al final d'aquestes línies:

    deb http://deb.debian.org/debian/ bullseye main non-free

    deb-src http://deb.debian.org/debian/ bullseye main non-free


    deb http://security.debian.org/debian-security bullseye-security main non-free

    deb-src http://security.debian.org/debian-security bullseye-security main non-f>


    # bullseye-updates, to get updates before a point release is made;

    # see https://www.debian.org/doc/manuals/debian-reference/ch02.en.html#_updates>

    deb http://deb.debian.org/debian/ bullseye-updates main non-free

    deb-src http://deb.debian.org/debian/ bullseye-updates main non-free

Y executar les següents comandes:

` apt search iwlwifi `

` sudo apt install firmaware-iwlwifi `

` sudo modprobe -r iwlwifi `

` sudo modprobe -r iwlwifi `

` sudo modprobe iwlwifi `
