# Virtualització


## KVM

Es un software que forma parte del Kernel de Linux i serveix per virtualitzar.


## QEMU

QEMU és un emulador de processadors. Executa el codi original com si fos a la màquina que està emulant amb la seva arquitectura. 


## QEMU - KVM

Quan ambdós prorgames s'utilitzen junts, resulta en en emulador de màquines (QEMU) que utilitza un processador virtualitzat amb KVM.



## Instal·lació de QEMU

``` sudo apt install qemu-kvm ```

Instal·lem també les seves llibreries que ens permetran modificar els discs virtuals:

``` sudo apt install libvirt-clients libvirt-daemon-system```

---

S'ha de tenir en compte que si tenim corrent el contenidor d'hipervisor d'IsardVDI hem d'aturar-lo, ja que influeix en la virtualització.

Arranquen aquest servei:

```systemctl start libvirtd```

## Creació de màquines virtuals

Per saber quins sistemes operatius podem virtualitzar podem fer la comanda:

```virt-builder --list```

El virt-builder és una eina per a construir una màquina virtual 

... falta cosas